char cmotor = 'c';
unsigned long now;
const float pi = 3.1415926;

//Input interpretation
bool reading = false;
int charsRead = 0;
byte num = 0;
byte anum = 127;
byte bnum = 127;

void setup() {
  Serial.begin(9600);
  Serial1.begin(9600);
  Serial2.begin(9600);
  Serial1.write(127);
  Serial2.write(127);
  Serial.println("Ready...");
  Serial.println("Pitch, Yaw  - (A, B)");
}

void loop() {
  if (Serial.available()) {
    while (1) {
      char in = Serial.read();
      if (in == -1) continue; //if no char in buffer
      if (in == ';') {
        both(127);
        Serial.println("DONE");
      }
      //Serial.print("Typed: ");
      //Serial.println(in);
      if (in == 10 & reading) {
        if (cmotor == 'a') {
          setMotor(num);
        } else if (cmotor == 'b') {
          setMotor(num);
        } else {Serial.println("NO MOTOR");}
        charsRead = 0;
        num = 0;
        reading = false;
      }
      if (reading) {
        //Serial.print("Num before is ");
        //Serial.println(num);
        if (charsRead++ < 4) {
          num = ((int)num)*10L+((int)(in-48));
        }
        //Serial.print("Num after is ");
        //Serial.println(num);
      }
      if (in == 'a' || in == 'b') {
        cmotor = in;
        reading = true;
        break;
      }
      
    }
  }
}

void setMotor(byte s) {
  if (cmotor == 'a') {
    pitch(s);
  } else if (cmotor == 'b') {
    yaw(s);
  } else {Serial.println("NO MOTOR");}
  Serial.print("  ");
  Serial.print(anum); //far, close
  Serial.print(", ");
  Serial.println(bnum);
}

void yaw(byte v) {
  bnum = v; 
  Serial2.write(v);
}
void pitch(byte v) {
  anum = v;
  Serial1.write(v);
}
void both(byte v) {
  yaw(v);
  pitch(v);
}
